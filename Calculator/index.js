const OPERATORS = ['+', '-', '/', '*'];
let screen = document.getElementById('screen');
screenValue=""

const equalBtn = () => {
    screen.value = eval(screen.value);
}
const clearScreen = () => {
    screen.value = "";
}
const addOperation = (intBtn) => {
    screenValue += intBtn;
    const lastExp = screen.value[screen.value.length - 1];
    if(!(OPERATORS.includes(intBtn ) && OPERATORS.includes( lastExp))){
        screen.value += intBtn;
   } 
} 
const delBtn=()=>{
    let totalLength=screen.value;
    screen.value = totalLength.slice(0, -1);
}